package com.example.demo.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Encounter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patients patients;
    private  String notes;
    private String visitCode;
    private String provider;
    private String billingCode;
    private String icd10;
    private Number totalCost;
    private Number copay;
    private String chiefComplaint;
    private Number pulse;
    private Number systolic;
    private Number diastolic;
    private LocalDate date;

    public Encounter(){}

    public Encounter(Long id, Patients patients, String notes, String visitCode, String provider, String billingCode, String icd10, Number totalCost, Number copay, String chiefComplaint, Number pulse, Number systolic, Number diastolic, LocalDate date) {
        this.id = id;
        this.patients = patients;
        this.notes = notes;
        this.visitCode = visitCode;
        this.provider = provider;
        this.billingCode = billingCode;
        this.icd10 = icd10;
        this.totalCost = totalCost;
        this.copay = copay;
        this.chiefComplaint = chiefComplaint;
        this.pulse = pulse;
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getVisitCode() {
        return visitCode;
    }

    public void setVisitCode(String visitCode) {
        this.visitCode = visitCode;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getIcd10() {
        return icd10;
    }

    public void setIcd10(String icd10) {
        this.icd10 = icd10;
    }

    public Number getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Number totalCost) {
        this.totalCost = totalCost;
    }

    public Number getCopay() {
        return copay;
    }

    public void setCopay(Number copay) {
        this.copay = copay;
    }

    public String getChiefComplaint() {
        return chiefComplaint;
    }

    public void setChiefComplaint(String chiefComplaint) {
        this.chiefComplaint = chiefComplaint;
    }

    public Number getPulse() {
        return pulse;
    }

    public void setPulse(Number pulse) {
        this.pulse = pulse;
    }

    public Number getSystolic() {
        return systolic;
    }

    public void setSystolic(Number systolic) {
        this.systolic = systolic;
    }

    public Number getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Number diastolic) {
        this.diastolic = diastolic;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
