package com.example.demo.services;

import com.example.demo.entities.Encounter;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface EncounterService {

    ResponseEntity<Encounter> getEncounterById(Long id);
    ResponseEntity<Encounter>addNewEncounter(Encounter encounter);

    ResponseEntity<Encounter>updateEncounterById(Encounter encounter);
    ResponseEntity<String>deleteEncounterById(Long id);


}
