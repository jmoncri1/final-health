package com.example.demo.services;

import com.example.demo.entities.Patients;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PatientService {
    List<Patients> getAllPatients();
    ResponseEntity<Patients> getPatientById(Long id);

    ResponseEntity<Patients> addNewPatient(Patients patients);

    ResponseEntity<Patients> updatePatientById(Patients patients);

    ResponseEntity<String> deletePatientById(Long id);
}
