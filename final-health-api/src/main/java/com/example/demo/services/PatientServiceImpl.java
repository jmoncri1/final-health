package com.example.demo.services;

import com.example.demo.entities.Patients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.example.demo.repository.PatientsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientServiceImpl implements PatientService{

    @Autowired
    private PatientsRepository patientRepository;

    @Override
    public List<Patients> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public ResponseEntity<Patients> getPatientById(Long id) {
        Optional<Patients> patients = patientRepository.findById(id);
        ResponseEntity<Patients> responseEntity;
        if (patients.isPresent()) {
            responseEntity = new ResponseEntity<>(patients.get(), HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Patients> addNewPatient(Patients patients) {

        Patients savedPatient = patientRepository.save(patients);
        ResponseEntity<Patients> responseEntity = new ResponseEntity<>(savedPatient, HttpStatus.CREATED);
        return  responseEntity;
    }

    @Override
    public ResponseEntity<Patients> updatePatientById(Patients patients) {
        Optional<Patients> possiblePatient = patientRepository.findById(patients.getId());

        ResponseEntity<Patients> responseEntity;
        if(!possiblePatient.isPresent()){
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Patients savedPatient = patientRepository.save(patients);
        responseEntity = new ResponseEntity<>(savedPatient, HttpStatus.OK);
        return responseEntity;
    }

    @Override
    public ResponseEntity<String> deletePatientById(Long id) {
        patientRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
