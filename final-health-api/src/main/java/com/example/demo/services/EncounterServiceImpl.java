package com.example.demo.services;

import com.example.demo.entities.Encounter;
import com.example.demo.repository.EncounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;



import java.util.Optional;

@Service
public class EncounterServiceImpl implements EncounterService{

    @Autowired
    private EncounterRepository encounterRepository;

    @Override
    public ResponseEntity<Encounter> getEncounterById(Long id) {
        Optional<Encounter> encounter = encounterRepository.findById(id);
        ResponseEntity<Encounter> responseEntity;
        if (encounter.isPresent()) {
            responseEntity = new ResponseEntity<>(encounter.get(), HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Encounter> addNewEncounter(Encounter encounter) {
        Optional<Encounter> possibleEncounter = encounterRepository.findById(encounter.getId());

        ResponseEntity<Encounter> responseEntity;
        if(!possibleEncounter.isPresent()) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Encounter savedEncounter = (Encounter) encounterRepository.save(encounter);
        responseEntity = new ResponseEntity<>(savedEncounter, HttpStatus.CREATED);
        return responseEntity;
    }

    @Override
    public ResponseEntity<Encounter> updateEncounterById(Encounter encounter) {
        Optional<Encounter> possibleEncounter = encounterRepository.findById(encounter.getId());

        ResponseEntity<Encounter> responseEntity;
        if(!possibleEncounter.isPresent()){
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Encounter savedEncounter = (Encounter) encounterRepository.save(encounter);
        responseEntity = new ResponseEntity<>(savedEncounter, HttpStatus.CREATED);
        return responseEntity;
    }

    @Override
    public ResponseEntity<String> deleteEncounterById(Long id) {
        encounterRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
