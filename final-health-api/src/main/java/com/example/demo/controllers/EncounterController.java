package com.example.demo.controllers;

import com.example.demo.entities.Encounter;
import com.example.demo.services.EncounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/encounters")
@CrossOrigin(origins = "*")
public class EncounterController {

    @Autowired
    private EncounterService encounterService;

    @GetMapping(value = "id/{id}")
    public ResponseEntity<Encounter> getEncounterById(@PathVariable Long id) {
        return encounterService.getEncounterById(id);
    }

    @PostMapping
    public ResponseEntity<Encounter> addNewEncounter(@RequestBody Encounter encounter) {
        return encounterService.addNewEncounter(encounter);
    }
    @PutMapping
    public ResponseEntity<Encounter> updateEncounter(@RequestBody Encounter encounter) {
        return encounterService.updateEncounterById(encounter);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEncounterById(@PathVariable Long id) {
        encounterService.deleteEncounterById(id);
    }


}
