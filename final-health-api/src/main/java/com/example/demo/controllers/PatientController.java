package com.example.demo.controllers;

import com.example.demo.entities.Patients;
import com.example.demo.repository.PatientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.services.PatientService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/patients")
@CrossOrigin(origins = {"http://localhost:3000"})
public class PatientController {

    private final String JSON = MediaType.APPLICATION_JSON_VALUE;
    @Autowired
    private PatientService patientService;

    @GetMapping
    public ResponseEntity<List<Patients>> getAllPatients() {
        List<Patients> patients = patientService.getAllPatients();
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Patients> getPatientById(@PathVariable Long id) {

        return patientService.getPatientById(id);
    }

    @PostMapping(produces = {JSON}, consumes = {JSON})
    public ResponseEntity<Patients> postNewPatient(@RequestBody Patients patients) {

        return patientService.addNewPatient(patients);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Patients> updatePatientById(@RequestBody Patients patients, @PathVariable Long id) {

        return patientService.updatePatientById(patients);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePatientById(@PathVariable Long id) {
        patientService.deletePatientById(id);
    }

}
