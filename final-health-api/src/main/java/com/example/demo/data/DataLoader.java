package com.example.demo.data;

import com.example.demo.entities.Encounter;
import com.example.demo.entities.Patients;
import com.example.demo.repository.EncounterRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.example.demo.repository.PatientsRepository;

import java.io.InputStream;
import java.time.LocalDate;

@Component
public class DataLoader implements CommandLineRunner {

    private Patients patient1;
    @Autowired
    private PatientsRepository patientsRepository;
    @Autowired
    private EncounterRepository encounterRepository;

    @Override
    public void run(String... args) throws Exception {

        loadPatients();
        loadEncounters();
    }

    private void  loadPatients() {
        patient1 = new Patients("Troy", "Harrison", "123456789", "tharrison@email.com", "123 patient1 ave", "Charlotte", "NC", "12345", 1, 36, 24, "Optum", "Male");
        patientsRepository.save(patient1);
    }

    private void loadEncounters() {
        Encounter encounter1 = new Encounter(1L, patient1, "He is healthy", "203", "Dr. Moncrieft", "201", "Z00", 20, 20, "Well child visit", 90, 90, 65, LocalDate.of(2022,12,12));
        encounterRepository.save(encounter1);
    }

//    private void loadEncounters(){
//        ObjectMapper objectMapper = new ObjectMapper();
//        try(InputStream inputStream = DataLoader.class.getResourceAsStream("encounterData.json")) {
//            Encounter encounter = objectMapper.readValue(inputStream.readAllBytes(), Encounter.class);
//            encounterRepository.save(encounter);
//        }catch (Exception ex){
//            ex.printStackTrace();
//            System.out.println("got here error in load encounters " + ex.getMessage());
//        }

}
