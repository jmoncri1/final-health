import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";


export default function EditPatient() {

    const [patient, setPatient] = useState({});
    const { id } = useParams("id");
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [ssn, setSsn] = useState("")
    const [email, setEmail] = useState("")
    const [street, setStreet] = useState("")
    const [city, setCity] = useState("")
    const [state, setState] = useState("")
    const [postal, setPostal] = useState("")
    const [age, setAge] = useState("")
    const [weight, setWeight] = useState("")
    const [insurance, setInsurance] = useState("")
    const [gender, setGender] = useState("")

  useEffect(() => {
    async function fetchData() {
      const getPatientById = await fetch(
        "http://localhost:8080/patients/" + id,
        {
          headers: new Headers({
            "Content-Type": "application/json",
            mode: "cors",
          }),
        }
      );
      const getPatient = await getPatientById.json();
      console.log(getPatient);
      setPatient(getPatient);
    }
    fetchData();
  }, [id]);



    return (
        <form>
            <label>Patient First Name</label>
            <input type="text" value={firstName} required onChange={(e) => setFirstName(e.target.value)}/>
            <label>Last Name</label>
            <input type="text" value={lastName} required onChange={(e) => setLastName(e.target.value)}/>
            <label>Social Security Number</label>
            <input type="number" value={ssn} required onChange={(e) => setSsn(e.target.value)} />
            <label>Email</label>
            <input type="text" value={email} required onChange={(e) => setEmail(e.target.value)} />
            <label>Street Address</label>
            <input type="text" value={street} required onChange={(e) => setStreet(e.target.value)} />
            <label>City</label>
            <input type="text" value={city} required onChange={(e) => setCity(e.target.value)} />
            <label>State</label>
            <input type="text" value={state} required onChange={(e) => setState(e.target.value)} />
            <label>Postal Code</label>
            <input type="number" value={postal} required onChange={(e) => setPostal(e.target.value)} />
            <label>Age</label>
            <input type="number" value={age} required onChange={(e) => setAge(e.target.value)} />
            <label>Weight in Pounds</label>
            <input type="number" value={weight} required onChange={(e) => setWeight(e.target.value)} />
            <label>Insurance</label>
            <input type="text" value={insurance} required onChange={(e) => setInsurance(e.target.value)} />
            <label>Gender</label>
            <input type="text" value={gender} required onChange={(e) => setGender(e.target.value)} />
        </form>
    )


}