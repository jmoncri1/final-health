import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, Navigate, useNavigate, useParams } from "react-router-dom";

export default function CreatePatient() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [ssn, setSsn] = useState("");
  const [email, setEmail] = useState("");
  const [street, setStreet] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [postal, setPostal] = useState("");
  const [age, setAge] = useState(0);
  const [height, setHeight] = useState(0);
  const [weight, setWeight] = useState(0);
  const [insurance, setInsurance] = useState("");
  const [gender, setGender] = useState("");

  const navigate = useNavigate();

  const patient = {
    firstName: firstName,
    lastName: lastName,
    ssn: ssn,
    email: email,
    street: street,
    city: city,
    state: state,
    postal: postal,
    age: age,
    height: height,
    weight: weight,
    insurance: insurance,
    gender: gender,
  };

    // async function handleSubmit(patient) {
    //   const fetchData = await fetch("http://localhost:8080/patients", {
    //     method: "POST",
    //     body: JSON.stringify(patient),
    //     headers: new Headers({
    //       "Content-Type": "application/json",
    //       "mode": "no-cors"
    //     }),
    //   });
    //   const response = await fetchData.json();
    //   console.log(fetchData);
    //   console.log(response);
    // }

    const headers = {
        "Content-Type": "application/json",
         "Access-Control-Allow-Origin": "*"
    };
    

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(patient);
    await axios.post("http://localhost:8080/patients/", patient, headers);
    navigate("/home");
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="firstName">Patient First Name</label>
      <input
        id="firstName"
        type="text"
        value={firstName}
        placeholder="ex. John"
        required
        onChange={(e) => setFirstName(e.target.value)}
      />
      <label>Last Name</label>
      <input
        type="text"
        value={lastName}
        placeholder="ex. Jones"
        required
        onChange={(e) => setLastName(e.target.value)}
      />
      <label>Social Security Number</label>
      <input
        type="text"
        value={ssn}
        placeholder="ex. 000-00-000"
        required
        onChange={(e) => setSsn(e.target.value)}
      />
      <label>Email</label>
      <input
        type="text"
        value={email}
        placeholder="ex. Jones@email.com"
        required
        onChange={(e) => setEmail(e.target.value)}
      />
      <label>Street Address</label>
      <input
        type="text"
        value={street}
        placeholder="ex. 123 street avenue"
        required
        onChange={(e) => setStreet(e.target.value)}
      />
      <label>City</label>
      <input
        type="text"
        value={city}
        placeholder="ex. Los Angeles"
        required
        onChange={(e) => setCity(e.target.value)}
      />
      <label>State</label>
      <input
        type="text"
        value={state}
        placeholder="ex. CA"
        required
        onChange={(e) => setState(e.target.value)}
      />
      <label>Postal Code</label>
      <input
        type="number"
        value={postal}
        placeholder="ex. 12345"
        required
        onChange={(e) => setPostal(e.target.value)}
      />
      <label>Age</label>
      <input
        type="number"
        value={age}
        placeholder="ex. 20"
        required
        onChange={(e) => setAge(e.target.value)}
      />
      <label>Height</label>
      <input
        type="number"
        value={height}
        placeholder="ex. 66"
        required
        onChange={(e) => setHeight(e.target.value)}
      />
      <label>Weight in Pounds</label>
      <input
        type="number"
        value={weight}
        placeholder="ex. 125"
        required
        onChange={(e) => setWeight(e.target.value)}
      />
      <label>Insurance</label>
      <input
        type="text"
        value={insurance}
        placeholder="ex. Humana"
        required
        onChange={(e) => setInsurance(e.target.value)}
      />
      <label>Gender</label>
      <input
        type="text"
        value={gender}
        placeholder="ex. male"
        required
        onChange={(e) => setGender(e.target.value)}
      />
      <button type="submit">Create</button>
    </form>
  );
}
