import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Home() {
  const [patients, setPatients] = useState([]);

  useEffect(() => {
    getAllPatients();
  }, []);

  const getAllPatients = async () => {
    const result = await axios.get("http://localhost:8080/patients");
    setPatients(result.data)
  }

  // useEffect(() => {
  //   async function fetchData() {
  //     const getAllPatients = await fetch("http://localhost:8080/patients", {
  //       headers: new Headers({
  //         "Content-Type": "application/json",
  //         mode: "cors",
  //       }),
  //     });
  //     const allPatients = await getAllPatients.json();
  //     console.log(allPatients);
  //     setPatients(allPatients);
  //   }
  //   fetchData();
  // }, []);

  return (
    <div>
      <h1>Patients</h1>

      <div className="patient-list">
        {patients.length > 0
          ? patients.map((patient) => {
              return (
                <div className="list-group-item" key={patient.id}>
                  <Link to={`/patient-detail/${patient.id}`}>
                    <p className="mb-1 text-center">
                      Patient Name: {patient.firstName}, {patient.lastName}
                    </p>
                  </Link>
                </div>
              );
            })
          : ""}
      </div>
      <Link to="/create-patient">
        <button className=" ">Add A New Patient</button>
      </Link>
    </div>
  );
}
