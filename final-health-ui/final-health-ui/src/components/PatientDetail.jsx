import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";

export default function PatientDetail() {
  const [patient, setPatient] = useState({});
  const { id } = useParams("id");

  useEffect(() => {
    getPatientById();
  }, [id]);

  const getPatientById = async () => {
    const result = await axios.get("http://localhost:8080/patients/" + id);
    setPatient(result.data);
  };

//   useEffect(() => {
//     async function fetchData() {
//       const getPatientById = await fetch(
//             "http://localhost:8080/patients/" + id,
//         {
//           headers: new Headers({
//             "Content-Type": "application/json",
//             mode: "cors",
//           }),
//         }
//       );
//       const getPatient = await getPatientById.json();
//       console.log(getPatient);
//       setPatient(getPatient);
//     }
//     fetchData();
//   }, [id]);

  return (
    <div>
      <h1>Patient Details</h1>

      <div className="patient-list">
        {Object.keys(patient).length > 0 ? (
          <div className="list-group-item" key={patient.id}>
            <p className="mb-1 text-center">
              Patient Name: {patient.firstName}
            </p>
            <p className="mb-1 text-center">Last Name: {patient.lastName}</p>
            <p className="mb-1">SSN: {patient.ssn}</p>
            <p className="mb-1">Email: {patient.email}</p>
            <p className="mb-1">Street Address: {patient.street}</p>
            <p className="mb-1">City: {patient.city}</p>
            <p className="mb-1">State: {patient.state}</p>
            <p className="mb-1">Zip Code: {patient.postal}</p>
            <p className="mb-1">Age: {patient.age}</p>
            <p className="mb-1">Height: {patient.height}</p>
            <p className="mb-1">Weight: {patient.weight}</p>
            <p className="mb-1">Insurance Carrier: {patient.insurance}</p>
            <p className="mb-1">Gender: {patient.gender}</p>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
