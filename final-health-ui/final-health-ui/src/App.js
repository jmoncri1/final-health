import "./App.css";
import { Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import CreatePatient from "./components/CreatePatient";
import PatientDetail from "./components/PatientDetail";

function App() {
  return (
    <div className="App">
      <h1>FINAL HEALTH</h1>
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/create-patient" element={<CreatePatient />} />
        <Route path="/patient-detail/:id" element={<PatientDetail />} />
      </Routes>
    </div>
  );
}

export default App;
